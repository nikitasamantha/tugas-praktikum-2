package com.nikitasamantha_10191065.tugaspraktikum2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    ImageView iv;
    Button btn;
    boolean status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        iv = findViewById(R.id.iv);
        btn = findViewById(R.id.btn);
        status = false;

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOff();
            }
        });

    }

    public void onOff(){
        if (!status){
            iv.setImageResource(R.drawable.LampuOn);
            status = true;
        } else {
            iv.setImageResource(R.drawable.LampuOff);
            status = false;
        }
    }

}